/* Main project file. */

// Import section.
const express = require('express');
const fs = require('fs');
const myLog = require('./myLog');
const open = require('open');
const portfinder = require('portfinder');
const syncRequest = require('sync-request');

// Instanciate the web server.
const app = express();

/* Returns the list of unique visitors based on the nginx log files.
 *
 * @returns {string[]} - the list of unique visitors IP addresses.
 */
app.get('/api/visitors', function (req, res) {
  let visitors = myLog.getVisitors();
  res.json(visitors);
});

/* Returns the (lat, long) of an ip.
 *
 * @param {string} ip - the ip address to resolve into lat,long.
 * @returns {Object} {'lon': {number}, 'lat': {number}}
 */
app.get('/api/pos', function (req, res) {
  let ip = req.query.ip;
  let url = 'http://ip-api.com/json/' + ip;
  console.log('ip to geodecode ->' + ip);
  let resIp = syncRequest('GET', url);
  data = resIp.getBody('utf8');
  data = JSON.parse(data);
  res.json({ 'lon': data['lon'], 'lat': data['lat'] });
});

/* Serves the static content */
app.use(express.static(__dirname));

/* Listen on the first port available 
 * then open the browser to this url.
 */
portfinder.getPortPromise()
  .then((portFound) => {
    let server = app.listen(portFound, function () {
      let host = server.address().address
      port = server.address().port
      console.log('app listening at: http://%s:%s', host, port)
      open('http://localhost:' + port);
    })
  })
  .catch((err) => {
    console.log("can't find an available port within: [" + portfinder.basePort + ';' + portfinder.highestPort + ']');
  });
