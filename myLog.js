/* Module to handle the nginx log files. */

const fs = require('fs');
const path = require('path');

/* Return the list of log filenames in the current directory. */
function getLogFiles() {
  let files = [];
  try {
    const arrayOfFiles = fs.readdirSync(__dirname);
    for (i = 0; i < arrayOfFiles.length; i++) {
      var reg = new RegExp('^access.log.*');
      var res = reg.test(arrayOfFiles[i]);
      if (res)
        files.push(arrayOfFiles[i]);
    }
  } catch (e) {
    console.log(e)
  }
  return files;
}

/* Return an array of visitors extracted from the log files.
 *
 * Example of output:
 * [ {'ip': '1.3.5.7', 'timestamp': '[1/Jan/2000:01:02:300 +0100]'},
 *   {'ip': '9.7.5.3', 'timestamp': '[1/Jan/2000:01:03:400 +0100]'}
 * ]
 */
exports.getVisitors = function () {
  files = getLogFiles();
  let visitors = [];

  files.forEach(file => {
    var lines = fs.readFileSync(file, 'utf-8')
      .split('\n')
      .filter(Boolean);

    lines.forEach(element => {
      visitor = {};
      let ip = element.substr(0, element.indexOf(' - - '));
      visitor['ip'] = ip;
      let tagStart = '- - [';
      let tagStop = '] ';
      let startPos = element.indexOf(tagStart) + tagStart.length;
      let length = element.indexOf(tagStop) - startPos;
      let timestamp = element.substr(startPos, length);
      visitor['timestamp'] = timestamp;
      visitors.push(visitor);
    });
  });
  return visitors;
}
