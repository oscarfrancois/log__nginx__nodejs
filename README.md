# Introduction

This project displays the visitors of a website into a map.

The list of visitors is extracted from the nginx log files
 (cf. http://nginx.org).

Example of rendering in the browser:

![visitors](img/rendering-example-800x405.jpg)

# Pre-requesites:

- Replace the example file access.log by your own nginx log files.
- nodejs must be installed.

# Setup

`npm i`
`npm start`

# Developer mode

In developer mode, the server is automatically reloaded after each change.

`npm run start-dev`


# Credits:

Leaflet for their map library: http://leafletjs.com

The icons are based on: https://github.com/pointhi/leaflet-color-markers
